# Aggy

Prototype for a plugin that makes your WP site a hub that will aggregate from other sites.

## Basic Idea

1.  "Connected Site" is a custom post type
2.  These posts have meta data like "department" and "year" and "type" so we can find and filter our "sites" like usual
3.  On site singles, all posts from the site are pulled in via the REST API

## Stuff humans will have to do:

1.  When a new site is created on the network you would also register it in wherever this plugin is running - the "registry" - it could be the root site on a multisite network, or not.

2.  If this plugin is running on the main site then it could be baked into site creation. But the idea is that your hub does not have to be your main site on a multisite network. But that's not done yet.

3.  When the user posts on their own site, they can/should attach taxonomy terms - maybe this could be controlled with a hook that does not let you publish a post until you have given it one from the required tax terms
