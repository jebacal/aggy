<?php
/*
Plugin Name: Aggy
Description: Get posts from a registered site
Version:     0.1
Author:      Joe Bacal, Smith College
License:     GPL2
License URI: https://www.gnu.org/licenses/gpl-2.0.html
*/

include 'postgetter.php';

//add the ajaxing business to the post
function add_aggy_content($content) {
    global $post;
    if (is_singular( 'eportfolio')){
        $post_getter = new postgetter($post);
        $post_getter->add_targets();
        $post_getter->establish_url();
        
        //localize vars out
        wp_localize_script( 'aggyscripts', 'aggyObj',
            array( 
                'url' => $post_getter->url
            )
        );
        return $content . $post_getter->html_to_render;
    }

    else {
        return $content;
    }

}
add_filter('the_content', 'add_aggy_content');

//add the js and styles
function aggy_styles_scripts(){
    wp_enqueue_style('aggystyles', plugins_url('aggy.css', __FILE__));
    //register because we will be localizing vars
    wp_register_script( 'aggyscripts', plugins_url( 'aggy.js' , __FILE__ ), array('jquery'), '', true );
    wp_enqueue_script( 'axios', plugins_url( 'axios.js' , __FILE__ ), '', '', true );
    wp_enqueue_script( 'aggyscripts', plugins_url( 'aggy.js' , __FILE__ ), array('jquery'), '', true );
}
add_action('wp_enqueue_scripts', 'aggy_styles_scripts');


//set up post type
function eportfolio_init() {
	$labels = array(
		'name'               => _x( 'eportfolio', 'post type general name' ),
		'singular_name'      => _x( 'eportfolio', 'post type singular name' ),
		'menu_name'          => _x( 'eportfolio', 'admin menu' ),
		'name_admin_bar'     => _x( 'eportfolio', 'add new on admin bar' ),
		'add_new'            => _x( 'Add New', 'eportfolio' ),
		'add_new_item'       => __( 'Add New eportfolio' ),
		'new_item'           => __( 'New eportfolio' ),
		'edit_item'          => __( 'Edit eportfolio' ),
		'view_item'          => __( 'View eportfolio' ),
		'all_items'          => __( 'All eportfolios' ),
		'search_items'       => __( 'Search eportfolio' ),
		'parent_item_colon'  => __( 'Parent eportfolio:' ),
		'not_found'          => __( 'No eportfolio found.' ),
		'not_found_in_trash' => __( 'No eportfolio found in Trash.' )
	);

	$args = array(
		'labels'             => $labels,
		'has archive'		 => true,
		'public'             => true,
		'menu_icon'			 => 'dashicons-welcome-widgets-menus',
		'publicly_queryable' => true,
		'show_ui'            => true,
		'show_in_menu'       => true,
		'show_in_nav_menus' 	=> true,
		'show_in_rest' => true,
		'query_var'          => true,
		'rewrite'            => array( 'slug' => 'eportfolio' ),
		'capability_type'    => 'post',
		'has_archive'        => true,
		'hierarchical'       => true,
		'menu_position'      => 2,
		'supports'           => array( 'title', 'editor', 'author')
	);
	register_post_type( 'eportfolio', $args );
}
add_action( 'init', 'eportfolio_init' );

function create_eportfolio_taxonomies() {

	$labels = array(
		'name'                       => _x( 'concentration', 'taxonomy general name' ),
		'singular_name'              => _x( 'concentration', 'taxonomy singular name' ),
		'search_items'               => __( 'Search concentrations' ),
		'popular_items'              => __( 'Popular concentrations' ),
		'all_items'                  => __( 'All concentrations' ),
		'parent_item'                => null,
		'parent_item_colon'          => null,
		'edit_item'                  => __( 'Edit concentration' ),
		'update_item'                => __( 'Update concentration' ),
		'add_new_item'               => __( 'Add New concentration' ),
		'new_item_name'              => __( 'New concentration' ),
		'separate_items_with_commas' => __( 'Separate concentrations with commas' ),
		'add_or_remove_items'        => __( 'Add or remove concentrations' ),
		'choose_from_most_used'      => __( 'Choose from the most used concentrations' ),
		'not_found'                  => __( 'No concentrations found.' ),
		'menu_name'                  => __( 'concentrations' ),
	);

	$args = array(
		'hierarchical'          => true,
		'labels'                => $labels,
		'show_ui'               => true,
		'show_admin_column'     => true,
		'show_in_nav_menus' 	=> true,
		'update_count_callback' => '_update_post_term_count',
        'query_var'             => true,
        'meta_box_cb'           => false,
		'rewrite'               => array( 'slug' => 'concentration' ),
	);
    register_taxonomy( 'concentration', 'eportfolio', $args );
}
add_action( 'init', 'create_eportfolio_taxonomies', 0 );
