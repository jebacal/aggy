<?php 

class Postgetter {
    public function __construct($post){
        $this->post = $post;
        $this->portfolio_key = get_field('site_key', $this->post->ID);
        $this->portfolio_url = get_field('site_url', $this->post->ID);
    }

    function add_targets(){
        $this->html_to_render = '<div class="aggy"><ul class="aggy-target"></ul></div>';
    }

    function establish_url(){
        //abstracted for now because we may be looping over multples later and using params
        $this->urlparams = 'posts';
        $this->url = $this->portfolio_url . '/wp-json/wp/v2/' . $this->urlparams;
    }
}

class Postagger {
    public function __construct($post){
        //code
    }
}