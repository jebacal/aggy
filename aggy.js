document.addEventListener("DOMContentLoaded", function () {
    axios.get(aggyObj.url)
        .then(function (response) {
            generateOutput(response);
        })
        .catch(function (error) {
            console.log(error);
        });
});


function generateOutput(response) {
    //console.log(response.data);
    for (let i = 0; i < response.data.length; i++) {
        //collect string values into vars
        var titleString = response.data[i].id + ': ' + response.data[i].link;

        //create an li to be appended
        var item = document.createElement("li");

        //create the string nodes to be appended to the items
        var textnode = document.createTextNode(titleString);

        //glue things up together
        item.appendChild(textnode);
        document.querySelector('.aggy-target').appendChild(item);
    }
}